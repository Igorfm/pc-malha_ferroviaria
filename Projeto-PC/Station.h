#ifndef STATION_H
#define STATION_H

#include <pthread.h>
#include <semaphore.h>
#include "Global.h"

typedef struct ListaEstacao{
    int tremId;
    struct ListaEstacao *next;
}ListaEstacao;

typedef struct{
    int tremId;
    int ocupado;
    int tempoPlat, tempoTrem;
    sem_t semPlataforma;
}Plataforma;

typedef struct{
    int stationID;
    char *name;
    int nPlat;
    ListaEstacao *lista;
    Plataforma *plataformas;
    pthread_mutex_t lockEnter, lockExit;
    pthread_t entraceControl;
    pthread_t exitControl;
}Station;

Station *stations;

pthread_t stationPthread[NODES];

void * StationThread(void *arg);

void AskPlaceStation(int, int);

void ExitStation(int, int);
#endif // STATION_H
