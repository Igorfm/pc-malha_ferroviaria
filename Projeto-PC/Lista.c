#include "Lista.h"

/* Cria uma lista vazia e a retorna */
ListaEstacao *lCria()
{
	return NULL;
}

/* Insere um novo elemento no fim da lista, usando o tremId passado como parametro */
ListaEstacao *lInsereFim( ListaEstacao *listaEstacao, int tremId )
{
	ListaEstacao *novo;
    ListaEstacao *auxiliar = listaEstacao;
    ListaEstacao *anterior = NULL;

    novo = ( ListaEstacao * ) malloc( sizeof( ListaEstacao ) );

    /* Verifica se o novo elemento foi alocado */
	if( !novo )
	{
		printf( "ERRO: Memoria insuficiente para novo elemento da lista!\n" );
		exit ( 1 );
	}

	novo->tremId = tremId;
	novo->next = NULL;

	/* Percorre a lista ate chegar ao seu final */
    while( auxiliar )
    {
    	anterior = auxiliar;
    	auxiliar = auxiliar->next;
    }

    /* Verifica se a lista esta vazia */
    if( !anterior )
    	listaEstacao = novo;
    else
    	anterior->next = novo;

    return listaEstacao;
}

/* Insere um novo elemento na lista de forma ordenada, usando o tremId passado como parametro */
ListaEstacao *lInsereOrdenado( ListaEstacao *listaEstacao, int tremId )
{
	ListaEstacao *novo;
	ListaEstacao *auxiliar = listaEstacao;
	ListaEstacao *anterior = NULL;

	novo = ( ListaEstacao * ) malloc( sizeof( ListaEstacao ) );

	/* Verifica se o novo elemento foi alocado */
	if( !novo )
	{
		printf( "ERRO: Memoria insuficiente para novo elemento da lista!\n" );
		exit ( 1 );
	}

	novo->tremId = tremId;

	/* Percorre a lista ate encontrar um elemento com tremId maior do que o que sera inserido ou ate chegar ao seu final */
	while( auxiliar != NULL && auxiliar->tremId < tremId )
	{
		anterior = auxiliar;
		auxiliar = auxiliar->next;
	}

	/* Verifica se a lista esta vazia ou se o tremId do primeiro elemento da lista eh maior do que o que sera inserido */
	if( !anterior )
	{
		novo->next = listaEstacao;
		listaEstacao = novo;
	}
	else
	{
		novo->next = anterior->next;
		anterior->next = novo;
	}

	return listaEstacao;
}

/* Imprime o tremId dos elementos da lista */
void lImprime( ListaEstacao *listaEstacao )
{
	printf( "Valores dos elementos da lista:\n" );

	/* Verifica se a lista esta vazia */
	if( !listaEstacao )
		printf( "Nenhum elemento na lista!\n" );
	else
	{
		ListaEstacao *auxiliar;

		/* Percorre a lista a patir do primeiro elemento ate que chegue ao ultimo elemento */
		for( auxiliar = listaEstacao; auxiliar != NULL; auxiliar = auxiliar->next )
		{
			if( auxiliar != listaEstacao )
				printf( ", " );

			printf( "%d", auxiliar->tremId );
		}

		printf( "\n" );
	}
}

/* Remove um elemento do inicio da lista */
ListaEstacao *lRemoveInicio( ListaEstacao *listaEstacao )
{
	ListaEstacao *auxiliar = listaEstacao;

	/* Verifica se a lista esta vazia */
	if( !listaEstacao )
	{
		printf( "ERRO: Lista vazia!\n" );
		exit( 1 );
	}

	listaEstacao = auxiliar->next;
	free( auxiliar );

	return listaEstacao;
}

/* Remove um elemento com o tremId passado como parametro */
ListaEstacao *lRemoveValor( ListaEstacao *listaEstacao, int tremId )
{
	ListaEstacao *anterior = NULL;
	ListaEstacao *auxiliar = listaEstacao;

	/* Verifica se a lista esta vazia */
	if( !listaEstacao )
	{
		printf( "ERRO: Lista vazia!\n" );
		exit( 1 );
	}

	/* Percorre a lista ate encontrar o elemento a ser removido ou ate chegar ao seu final */
	while( auxiliar != NULL && auxiliar->tremId != tremId )
	{
		anterior = auxiliar;
		auxiliar = auxiliar->next;
	}

	/* Verifica se o elemento com o tremId passado foi encontrado */
	if( !auxiliar )
	{
		printf("ERRO: Elemento com tremId %d nao encontrado na lista!\n", tremId );
		exit( 2 );
	}

	/* Verifica se o elemento a ser removido eh o primeiro da lista */
	if( anterior == NULL )
		listaEstacao = auxiliar->next;
	else
		anterior->next = auxiliar->next;

	free( auxiliar );

	return listaEstacao;
}

/* Destroi a lista */
void lDestroi( ListaEstacao *listaEstacao )
{
	ListaEstacao *auxiliar = listaEstacao;

	/* Percorre a lista, desalocando no por no */
	while( auxiliar )
	{
		ListaEstacao *next = auxiliar->next;

		free( auxiliar );
		auxiliar = next;
	}
}
