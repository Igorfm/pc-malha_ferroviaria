#include <curses.h>
#include <stdio.h>
#include <unistd.h>
#include <termios.h>
#include <fcntl.h>
#include <sys/select.h>
#include <stropts.h>
#include "Global.h"
#include "Trem.h"
#include "Station.h"

char **stringEstado = (char *[]){"Na estacao" ,"Andando", "Esperando"};
char **stringPlat   = (char *[]){"Livre", "Ocupado"};
void (*foo)();

void sair()
{
    endwin(); /*Sempre que finalizarmos um programa com a biblioteca curses,
                     devemos executar este comando.*/
}

void initializeCurses(){
    initscr();
    start_color();
    init_pair(1,COLOR_WHITE,COLOR_BLUE);
    bkgd(COLOR_PAIR(1));


}

int getEstado(int estado){

    if(estado < 0){
        return estado * -1;
    }else{
        return 0;
    }
}

void printTrens(){
    int i, linha = 0, coluna = 1, count = 0;

    for(i = 0; i < NTRENS; i++){
        move( (linha++),coluna);
        printw("Trem %d     ", i);
        move((linha++),coluna);
        printw("Estado: %s       ", stringEstado[getEstado(trens[i].naEstacao)] );

        if(trens[i].path != NULL){
            move((linha++),coluna);
            printw("Saindo: %d       ", trens[i].path->stationFrom);
            move((linha++), coluna);
            printw("Indo: %d     ", trens[i].path->stationTo);
            move((linha++), coluna);
            printw("Estacao final: %d", trens[i].finalStation);
            move((linha++), coluna);
            printw("Custo: %d        ", trens[i].path->cost);
            move(linha++, coluna);
            printw("Plataforma %d: %s  ", trens[i].path->stationTo, stringPlat[stations[trens[i].path->stationTo].plataformas[0].ocupado]);

        }else{
            linha += 5;
        }

        move((linha++), 1);
        printw("            ");
        count++;
        if( count==3 ){
            coluna += 23;
            linha = 0;
            count = 0;
        }

    }

    refresh();
    sleep(1);
    //clear();
}

void  printStations(){
    int i, j, linha = 0, coluna = 1, count = 0;

    for(i = 0; i < NODES; i++){
        move( (linha++),coluna);
        printw("Estacão %d", i);
        move((linha++),coluna);
        if(stations[i].lista != NULL){
            printw("Próximo trem: %d    ", stations[i].lista->tremId);
        }else{
            printw("Próximo trem: Nenhum");
        }
        move((linha++),coluna);
        printw("Plataformas: %d", stations[i].nPlat);

        for(j = 0; j < stations[i].nPlat; j++){
            move((linha++),coluna);
            printw("Plataforma %d", j);
            move((linha++), coluna);
            printw("Estado: %s  ",stringPlat[stations[i].plataformas[j].ocupado]);
            move((linha++),coluna);
            if(stations[i].plataformas[j].ocupado == TRUE){
                printw("Trem ID: %d", stations[i].plataformas[j].tremId);
            }else{
                printw("Trem ID: Nenhum");
            }
            move((linha++), coluna);
            printw("Tempo: %d ", stations[i].plataformas[j].tempoTrem);

        }

        move((linha++), 1);
        printw("            ");
        count++;
        if( count==3 ){
            coluna += 28;
            linha = 0;
            count = 0;
        }

    }

    refresh();
    sleep(1);
    //clear();
}


void destroy_win(WINDOW *local_win)
{
	/* box(local_win, ' ', ' '); : This won't produce the desired
	 * result of erasing the window. It will leave it's four corners
	 * and so an ugly remnant of window.
	 */
	wborder(local_win, ' ', ' ', ' ',' ',' ',' ',' ',' ');
	/* The parameters taken are
	 * 1. win: the window on which to operate
	 * 2. ls: character to be used for the left side of the window
	 * 3. rs: character to be used for the right side of the window
	 * 4. ts: character to be used for the top side of the window
	 * 5. bs: character to be used for the bottom side of the window
	 * 6. tl: character to be used for the top left corner of the window
	 * 7. tr: character to be used for the top right corner of the window
	 * 8. bl: character to be used for the bottom left corner of the window
	 * 9. br: character to be used for the bottom right corner of the window
	 */
	wrefresh(local_win);
	delwin(local_win);
}

WINDOW *create_newwin(int height, int width, int starty, int startx)
{	WINDOW *local_win;

	local_win = newwin(height, width, starty, startx);
	box(local_win, 0 , 0);		/* 0, 0 gives default characters
					 * for the vertical and horizontal
					 * lines			*/
	wrefresh(local_win);		/* Show that box 		*/

	return local_win;
}

void WindowStart(WINDOW *my_win){


	int ch;
    void (*foo)();
    foo = &printTrens;

	initscr();			/* Start curses mode 		*/
	cbreak();			/* Line buffering disabled, Pass on
					 * everty thing to me 		*/
	keypad(stdscr, TRUE);		/* I need that nifty F1 	*/

	while(1)
	{
            timeout(100);
            ch = getch();
            clear();

		if(ch == KEY_LEFT){
            foo = &printStations;
            clear();
            printStations();
		}
		else if(ch == KEY_RIGHT){
            foo = &printTrens;
            clear();
            printTrens();
		}else if(ch == KEY_UP){
            clear();
		}else{
            foo();
		}
	}

	endwin();			/* End curses mode		  */
}


