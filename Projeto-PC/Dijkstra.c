#include "Dijkstra.h"
#include "Utils.h"
#include <stdlib.h>
#include <stdio.h>

Path* createPath(int from, int to){
    Path *novo;

    novo =(Path*) malloc(sizeof(Path));
    novo->stationFrom = from;
    novo->stationTo   = to;
    novo->cost        = GC[from][to];
    novo->next        = NULL;

    return novo;
}

Path* insertList(Path *lista, Path *novo){

    novo->next = lista;
    return novo;
}

int selectLessCost(DijkstraGraph *costs){

    int i, lessCost = INFINITY, stationId;

    for(i = 0; i < NODES; i++){
        if(costs[i].cost < lessCost && costs[i].cost > 0){
            lessCost = costs[i].cost;
            stationId = i;
        }
    }

    return stationId;
}

DijkstraGraph getLessCost(DijkstraGraph *costs){

    int i, lessCost = INFINITY;
    DijkstraGraph lessCostDG;

    for(i = 0; i < NODES; i++){
        if(costs[i].cost < lessCost && costs[i].cost > 0){
            lessCost = costs[i].cost;
            lessCostDG = costs[i];
        }
    }

    return lessCostDG;
}

DijkstraGraph getLessCostColumn(DijkstraGraph costs[NODES][NODES], int column){

    int i, lessCost = INFINITY;
    DijkstraGraph lessCostDG;

    for(i = 0; i < NODES; i++){
        if(costs[i][column].cost < lessCost && costs[i][column].cost > 0){
            lessCost = costs[i][column].cost;
            lessCostDG = costs[i][column];
        }
    }

    return lessCostDG;
}

int returnLessCost(DijkstraGraph *costs){

    int i, lessCost = INFINITY;

    for(i = 0; i < NODES; i++){
        if(costs[i].cost < lessCost && costs[i].cost > 0){
            lessCost = costs[i].cost;
        }
    }

    return lessCost;
}

Path* findPath(DijkstraGraph DG[NODES][NODES], int initialStation, int finalStation, int lastStation){

    Path *path = NULL;
    DijkstraGraph stationId;

    stationId = getLessCost(DG[lastStation]);
    path = insertList(path, createPath(stationId.from, stationId.to));

    while(path->stationFrom != initialStation){
        lastStation = path->stationFrom;
        stationId = getLessCostColumn(DG, lastStation);
        path = insertList(path, createPath(stationId.from, stationId.to));
    }

    return path;
}

Path* findPathDijkstra(int initialStation, int finalStation){

    //Dijkstra Graph
    DijkstraGraph DG[NODES][NODES];
    //Control node visited
    Cost NodeVisited[NODES];

    int i= 0, j = 0, currentStation = initialStation, lastStation= 0;
    int exit = 0;

    for(i = 0; i < NODES; i++){
       NodeVisited[i].wasVisited = notVisited;
    }

    NodeVisited[currentStation].stationId   = currentStation;
    NodeVisited[currentStation].cost        = 0;
    NodeVisited[currentStation].wasVisited  = Visited;

    for(i = 0; i < NODES; i++){
        for(j = 0; j < NODES; j++){
            DG[i][j].cost = INFINITY;
        }
    }


    while(!exit){

        for(i = 0; i < NODES; i++){
            if(NodeVisited[i].wasVisited == notVisited){
                if( (GC[currentStation][i] + NodeVisited[currentStation].cost) <=  DG[lastStation][i].cost){
                    DG[currentStation][i].cost = GC[currentStation][i] + NodeVisited[currentStation].cost;
                    DG[currentStation][i].from = currentStation;
                    DG[currentStation][i].to   = i;
                }else{
                    DG[currentStation][i].cost = DG[lastStation][i].cost;
                    DG[currentStation][i].from = DG[lastStation][i].from;
                    DG[currentStation][i].to   = DG[lastStation][i].to;
                }

            }else{
                if(i == currentStation ){
                    DG[currentStation][i].cost = 0;
                }else{
                    DG[currentStation][i].cost = INFINITY;
                }
            }
        }

        lastStation = currentStation;
        currentStation = selectLessCost(DG[currentStation]);

        NodeVisited[currentStation].stationId = currentStation;
        NodeVisited[currentStation].cost = returnLessCost(DG[lastStation]);
        NodeVisited[currentStation].wasVisited = Visited;


        if(currentStation == finalStation){
            exit = 1;
        }
    }

    return findPath(DG, initialStation, finalStation, lastStation);
}

Path* freePathCell(Path *lista){
    Path *ret = lista->next;
    free(lista);
    return ret;
}

void printfList(Path *lista){

    while(lista != NULL){
        printf("From Station: %d\n", lista->stationFrom);
        printf("To Station: %d\n", lista->stationTo);
        printf("Cost: %d\n", lista->cost);
        lista = lista->next;
    }
}

