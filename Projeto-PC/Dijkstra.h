#ifndef DIJKSTRA_H
#define DIJKSTRA_H

#include "Global.h"
#include <stdlib.h>

typedef enum Visitation{notVisited, Visited}Visitation;

typedef struct Node{

    int stationId, cost, wasVisited;
}Cost;

typedef struct Path{

    int stationFrom, stationTo;
    int cost;
    struct Path *next;
}Path;

typedef struct DijkstraGraph{

    int from, to;
    int cost;
}DijkstraGraph;

Path* findPathDijkstra(int, int );
Path* freePathCell(Path *);
Path* createPath(int, int);
Path* insertList(Path*, Path*);
void printfList(Path*);
#endif // DIJKSTRA_H
