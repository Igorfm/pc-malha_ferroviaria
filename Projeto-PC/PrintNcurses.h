#ifndef PRINTNCURSES_H
#define PRINTNCURSES_H

#include<curses.h>

void initializeCurses();
void WindowStart(WINDOW *);
WINDOW *create_newwin(int , int , int , int );
void printTrens();
#endif // PRINTNCURSES_H
