#ifndef FILA_H_INCLUDED
#define FILA_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

typedef struct node
{
	int value;
	struct node *next;
} Node;

typedef struct queue
{
	Node *front;
	Node *back;
} Queue;

Queue *create();

void push( Queue *, int );

void print( Queue * );

void pop( Queue * );

void destroy( Queue * );

#endif
