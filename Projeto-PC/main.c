#include <stdio.h>
#include <stdlib.h>
#include "Global.h"
#include "Initialization.h"
#include "Dijkstra.h"
#include "Utils.h"
#include "PrintNcurses.h"
#include "Station.h"

int main()
{
    int i;

    initializeCurses();
    Initialize();


    while(TRUE){
        //printTrens();
        //printStations();
        WindowStart(create_newwin(100, 100, 0, 0));
    }

    for(i = 0; i < NODES; i++){
        pthread_join((stationPthread[i]), NULL);
    }

    return 0;
}
