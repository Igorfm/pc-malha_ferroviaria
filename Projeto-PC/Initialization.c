#include "Initialization.h"
#include <stdlib.h>
#include <string.h>
#include <semaphore.h>
#include "Dijkstra.h"
#include "Station.h"
#include "Trem.h"
#include "Global.h"

void InitializeStations(){
    int i, j;
    stations = (Station *) malloc(sizeof(Station) * NODES);

    for(i = 0; i < NODES; i++){
        stations[i].stationID = i;
        stations[i].name = (char*) malloc(sizeof(char));
        stations[i].nPlat = 1;
        stations[i].lista = NULL;
        stations[i].plataformas = (Plataforma*) malloc(stations[i].nPlat * sizeof(Plataforma));
        pthread_mutex_init( &(stations[i].lockEnter), NULL);
        pthread_mutex_init( &(stations[i].lockExit), NULL);

        for(j = 0; j < stations[i].nPlat; j++)
        {
            stations[i].plataformas[j].ocupado = INITIALIZING;
            stations[i].plataformas[j].tempoPlat = 10;
            stations[i].plataformas[j].tempoTrem = stations[i].plataformas[j].tempoPlat;
            sem_init( &(stations[i].plataformas[j].semPlataforma), 0, 0);

        }

    }

    strcpy(stations[ID_A].name, "A");
    strcpy(stations[ID_B].name, "B");
    strcpy(stations[ID_C].name, "C");
    strcpy(stations[ID_D].name, "D");
    strcpy(stations[ID_E].name, "E");
    strcpy(stations[ID_F].name, "F");
    strcpy(stations[ID_G].name, "G");
    strcpy(stations[ID_H].name, "H");
}

void InitializeTrens(){
    int i;

    trens = (Trem*) malloc (NTRENS*sizeof(Trem));

    for(i = 0; i <NTRENS; i++){
        trens[i].path = NULL;
        insertList(trens[i].path, createPath(-1, ID_A));
        trens[i].naEstacao = ID_A;
        trens[i].init = TRUE;
        sem_init( &(trens[i].semaforo), 0, 0);
        AskPlaceStation(ID_A, i);
    }
}

void InitializeConnect(){

    /**     A                          B                           C                          D                          E                          F                          G                          H                         */
    /* A */ GC[ID_A][ID_A] = 0       ; GC[ID_A][ID_B] = 10       ; GC[ID_A][ID_C] = 20      ; GC[ID_A][ID_D] = INFINITY; GC[ID_A][ID_E] = INFINITY; GC[ID_A][ID_F] = INFINITY; GC[ID_A][ID_G] = INFINITY; GC[ID_A][ID_H] = INFINITY;
    /* B */ GC[ID_B][ID_A] = 10      ; GC[ID_B][ID_B] = 0        ; GC[ID_B][ID_C] = INFINITY; GC[ID_B][ID_D] = 35      ; GC[ID_B][ID_E] = 8       ; GC[ID_B][ID_F] = INFINITY; GC[ID_B][ID_G] = INFINITY; GC[ID_B][ID_H] = INFINITY;
    /* C */ GC[ID_C][ID_A] = 20      ; GC[ID_C][ID_B] = INFINITY ; GC[ID_C][ID_C] = 0       ; GC[ID_C][ID_D] = INFINITY; GC[ID_C][ID_E] = INFINITY; GC[ID_C][ID_F] = INFINITY; GC[ID_C][ID_G] = INFINITY; GC[ID_C][ID_H] = 5       ;
    /* D */ GC[ID_D][ID_A] = INFINITY; GC[ID_D][ID_B] = 35       ; GC[ID_D][ID_C] = INFINITY; GC[ID_D][ID_D] = 0       ; GC[ID_D][ID_E] = INFINITY; GC[ID_D][ID_F] = 26      ; GC[ID_D][ID_G] = 15      ; GC[ID_D][ID_H] = INFINITY;
    /* E */ GC[ID_E][ID_A] = INFINITY; GC[ID_E][ID_B] = 8        ; GC[ID_E][ID_C] = INFINITY; GC[ID_E][ID_D] = INFINITY; GC[ID_E][ID_E] = 0       ; GC[ID_E][ID_F] = 5       ; GC[ID_E][ID_G] = INFINITY; GC[ID_E][ID_H] = INFINITY;
    /* F */ GC[ID_F][ID_A] = INFINITY; GC[ID_F][ID_B] = INFINITY ; GC[ID_F][ID_C] = INFINITY; GC[ID_F][ID_D] = 26      ; GC[ID_F][ID_E] = 5       ; GC[ID_F][ID_F] = 0       ; GC[ID_F][ID_G] = INFINITY; GC[ID_F][ID_H] = INFINITY;
    /* G */ GC[ID_G][ID_A] = INFINITY; GC[ID_G][ID_B] = INFINITY ; GC[ID_G][ID_C] = INFINITY; GC[ID_G][ID_D] = 15      ; GC[ID_G][ID_E] = INFINITY; GC[ID_G][ID_F] = INFINITY; GC[ID_G][ID_G] = 0       ; GC[ID_G][ID_H] = 7       ;
    /* H */ GC[ID_H][ID_A] = INFINITY; GC[ID_H][ID_B] = INFINITY ; GC[ID_H][ID_C] = 5       ; GC[ID_H][ID_D] = INFINITY; GC[ID_H][ID_E] = INFINITY; GC[ID_H][ID_F] = INFINITY; GC[ID_H][ID_G] = 7       ; GC[ID_H][ID_H] = 0       ;
}

void InitializeThreads(){
    int i, *id;

    for(i = 0; i < NODES; i++){
        id = (int*) malloc(sizeof(int));
        *id = i;
        pthread_create(&(stationPthread[i]), NULL, StationThread, (void*)id);
    }

    for(i = 0; i < NTRENS; i++){
        id = (int*) malloc(sizeof(int));
        *id = i;
        pthread_create(&(tremPthread[i]), NULL, TremThread, (void*)id);
        //AskPlaceStation(ID_A, i);
    }
}

void Start(){
    int i, j;

    for(i = 0; i < NODES; i++){
        for(j = 0; j < stations[i].nPlat; j++){
            stations[i].plataformas[j].ocupado = FALSE;
        }
    }
}

void Initialize(){

    InitializeConnect();
    InitializeStations();
    InitializeTrens();
    InitializeThreads();
    Start();

}
