#ifndef GLOBAL_H
#define GLOBAL_H

#define TRUE 1
#define FALSE 0
#define NTRENS 9
#define NODES 8
#define INITIALIZING -3
#define ID_A 0
#define ID_B 1
#define ID_C 2
#define ID_D 3
#define ID_E 4
#define ID_F 5
#define ID_G 6
#define ID_H 7
#define INFINITY 100


//Graph Connections
int GC[NODES][NODES];
#endif // GLOBAL_H
