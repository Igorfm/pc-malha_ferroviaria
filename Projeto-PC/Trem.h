#ifndef TREM_H
#define TREM_H

#include <stdlib.h>
#include "Dijkstra.h"
#include <pthread.h>
#include <semaphore.h>

#define MOVING -1
#define WAITING -2

typedef struct {
    Path *path;
    int naEstacao, plataformaId, finalStation, init;
    sem_t semaforo;
    pthread_mutex_t lock;
}Trem;

pthread_t tremPthread[NTRENS];

Trem *trens;

void* TremThread(void*);

#endif // TREM_H
