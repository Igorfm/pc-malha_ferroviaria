#ifndef LISTA_H_INCLUDED
#define LISTA_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include "Station.h"

/* Cria uma lista vazia e a retorna */
ListaEstacao *lCria();

/* Insere um novo elemento no fim da lista, usando o tremId passado como parametro */
ListaEstacao *lInsereFim( ListaEstacao *, int );

/* Insere um novo elemento na lista de forma ordenada, usando o valor passado como parametro */
ListaEstacao *lInsereOrdenado( ListaEstacao *, int );

/* Imprime o valor dos elementos da lista */
void lImprime( ListaEstacao * );

/* Remove um elemento do inicio da lista */
ListaEstacao *lRemoveInicio( ListaEstacao * );

/* Remove um elemento com o valor passado como parametro */
ListaEstacao *lRemoveValor( ListaEstacao *, int );

/* Destroi a lista */
void lDestroi( ListaEstacao * );

#endif
