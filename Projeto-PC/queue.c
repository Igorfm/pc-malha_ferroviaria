#include "queue.h"

Queue *create()
{
	Queue *queue;
	
	queue = ( Queue * ) malloc( sizeof( Queue ) );
	
	if( !queue )
	{
		printf( "ERROR: Insufficient memory for new queue element!\n" );
		exit( 1 );
	}
	
	queue->front = queue->back = NULL;
	
	return queue;
}

void push( Queue *queue, int value )
{
	Node *new;
	
	new = ( Node * ) malloc( sizeof( Node ) );
	
	if( !new )
	{
		printf( "ERROR: Insufficient memory for new queue element!\n" );
		exit ( 1 );
	}
	
	new->value = value;
	new->next = NULL;
	
	if( !queue->front )
		queue->front = queue->back = new;
	else
	{
		queue->back->next = new;
		queue->back = new;
	}
}

void print( Queue *queue )
{
	printf( "Queue element values:\n" );
	
	if( !queue->front )
		printf( "No element in queue!\n" );
	else
	{
		Node *front;
		
		for( front = queue->front; front != NULL; front = front->next )
		{
			if( front != queue->front )
				printf( ", " );
			
			printf( "%d", front->value );
		}
		
		printf( "\n" );
	}
}

void pop( Queue *queue )
{
	Node *front = queue->front;
	
	if( !queue->front )
	{
		printf( "ERROR: Empty queue!\n" );
		exit( 1 );
	}
	
	queue->front = front->next;
	free( front );
	
	if( !queue->front )
		queue->back = NULL;
}

void destroy( Queue *queue )
{
	Node *front = queue->front;
	
	while( !front )
	{
		Node *next = front->next;
		free( front );
		front = next;
	}
	
	queue->front = queue->back = NULL;
	free( queue );	
}
