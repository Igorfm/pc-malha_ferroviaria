#include "Utils.h"
#include <stdlib.h>

/**
*   \brief Return the size of the pointer 'array'
*
*   @param array The array to count
*   @param sizeofTypeArray the sizeof the array type
*/
int countArrayPointer(void *array, int sizeofTypeArray){
    return (sizeof(array)/sizeofTypeArray + 1);
}

/**
*   \brief return a randon number from 0 to param limit
*
*
*
*/
int RandomLimit(int limit){
    return (rand() % limit);
}


