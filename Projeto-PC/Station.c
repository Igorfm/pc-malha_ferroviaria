#include "Station.h"
#include <unistd.h>
#include "Lista.h"
#include "Trem.h"

void * StationEntranceControl(void *arg){
    int id = *((int*)arg);
    int i = 0;
    while(TRUE){

        for(i = 0; i < stations[id].nPlat; i++){

            if( stations[id].plataformas[i].ocupado == FALSE )
            {
                if( stations[id].lista != NULL ){
                    sem_post( &(trens[stations[id].lista->tremId].semaforo) );
                    stations[id].plataformas[i].ocupado = TRUE;
                    stations[id].plataformas[i].tempoTrem = stations[id].plataformas[i].tempoPlat;
                    stations[id].plataformas[i].tremId = stations[id].lista->tremId;
                    trens[stations[id].lista->tremId].plataformaId = i;
                    stations[id].lista = lRemoveInicio(stations[id].lista);
                }
            }
            sleep(1);
        }

    }
}

void * StationExitControl(void *arg){
    int id = *((int*)arg);
    int i;

    while(TRUE){

        for(i = 0; i < stations[id].nPlat; i++){

            if(stations[id].plataformas[i].ocupado == TRUE){
                if(stations[id].plataformas[i].tempoTrem == 0){
                    sem_post( &(stations[id].plataformas[i].semPlataforma) );
                }else{
                stations[id].plataformas[i].tempoTrem -= 1;
                }
            }else{
                stations[id].plataformas[i].tempoTrem = stations[id].plataformas[i].tempoPlat;
            }

            sleep(1);
        }

    }
}

void AskPlaceStation(int estacaoId, int tremId){

    pthread_mutex_lock( &(stations[estacaoId].lockEnter) );

    stations[estacaoId].lista = lInsereFim(stations[estacaoId].lista, tremId);

    pthread_mutex_unlock( &(stations[estacaoId].lockEnter) );
}

void ExitStation(int estacaId, int tremId){

    if(estacaId >= 0){
        pthread_mutex_lock( &(stations[estacaId].lockExit) );

        stations[estacaId].plataformas[trens[tremId].plataformaId].ocupado = FALSE;

        stations[estacaId].plataformas[trens[tremId].plataformaId].tempoTrem = stations[estacaId].plataformas[trens[tremId].plataformaId].tempoPlat;
        pthread_mutex_unlock( &(stations[estacaId].lockExit) );
    }
}

void * StationThread(void *arg){
    int id = *((int*)arg);

    pthread_create(&(stations[id].entraceControl), NULL, StationEntranceControl, arg);
    pthread_create(&(stations[id].exitControl), NULL, StationExitControl, arg);

    pthread_join((stations[id].entraceControl), NULL);
    pthread_join((stations[id].exitControl), NULL);

    return 0;
}
