#include "Trem.h"
#include "Station.h"
#include "Utils.h"
#include <unistd.h>


void* TremThread(void *arg){
    int id = *((int*) arg);
    int i;

    while(1){

        if(trens[id].path == NULL){
            do{
                trens[id].finalStation = RandomLimit(NODES);
            }while(trens[id].finalStation == trens[id].naEstacao);
            trens[id].path = findPathDijkstra(trens[id].naEstacao, trens[id].finalStation);
        }


        //sem_wait( &(trens[id].semaforo) );    //Espera ser inserido na fila da pr�xima estac�o

        sem_wait( &(stations[trens[id].path->stationFrom].plataformas[trens[id].plataformaId].semPlataforma) );      //Espera libera��o da esta��o atual

        if(trens[id].init == TRUE){
            sem_wait( &(trens[id].semaforo) );
            trens[id].init = FALSE;
        }

        ExitStation(trens[id].path->stationFrom, id);    //Avisa a esta��o atual que sai
        trens[id].naEstacao = MOVING;
        int custo = trens[id].path->cost;
        for(i = custo; i > 0; i--){
            sleep(1);   //Andando
            trens[id].path->cost--;
        }

        trens[id].naEstacao = WAITING;
        AskPlaceStation(trens[id].path->stationTo, id);

        sem_wait( &(trens[id].semaforo) ); //Espera pra entrar na esta��o
        trens[id].naEstacao = trens[id].path->stationTo;    //Entrou

        trens[id].path = freePathCell(trens[id].path);
    }

}
